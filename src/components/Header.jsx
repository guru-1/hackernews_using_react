import React, { Component } from "react";
import { Navbar, Form, Button } from "react-bootstrap";
import logo from "../components/images/logo.png";
import settings from "../components/images/settings_black_24dp.svg";
import Body from "./Body";

export class Header extends Component {
  state = {
    searchString: "",
  };

  search = () => {
    const input = document.querySelector("input");
    this.setState({
      searchString: input.value,
    });
  };

  render() {
    return (
      <div>
        <Navbar expand="lg" style={navbarStyle}>
          <div>
            <div className="imageSection mx-2">
              <img
                className="logo"
                src={logo}
                alt=""
                style={{ height: "50%", width: "30%", float: "left" }}
              />
            </div>
            <div
              className="search_content my-1"
              style={{ fontSize: "1.2rem", fontWeight: "bold" }}
            >
              Search
              <br />
              Hacker News
            </div>
          </div>
          <Form className="mx-3" style={{ width: "78%" }}>
            <input
              type="search"
              placeholder="Search stories by url,title,author"
              style={{ width: "90%", outline: "none", padding: "0.5rem" }}
            ></input>
            <Button
              variant="outline-success"
              className="mx-2 md-2 searchButton"
              onClick={this.search}
            >
              Search
            </Button>
          </Form>
          <div className="settings d-flex">
            <div>
              <img
                src={settings}
                className="settings_icon"
                alt=""
                style={{ width: "200%" }}
              />
            </div>
            <div className="ml-4">
              <p
                className="my-2 lead"
                style={{ fontSize: "1.2rem", fontWeight: "bold" }}
              >
                Settings
              </p>
            </div>
          </div>
        </Navbar>
        <Body searchString={this.state.searchString} />
      </div>
    );
  }
}
const navbarStyle = {
  backgroundColor: "orange",
  minheight: "10vh",
  margin: "0 2rem 0 2rem",
};

export default Header;
