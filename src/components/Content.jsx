import React, { Component } from "react";
import Dataitem from "./Dataitem";

export class Content extends Component {
  render() {
    return (
      <div>
        <Dataitem data={this.props.data} />
      </div>
    );
  }
}

export default Content;
