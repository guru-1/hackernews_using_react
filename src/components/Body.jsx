import React, { Component } from "react";
import share from "../components/images/share_black_24dp.svg";
import Content from "./Content";
import axios from "axios";

export class Body extends Component {
  state = {
    data: [
      {
        title: "loading",
        url: "loading",
        num_comments: "loading",
        author: "loading",
        points: "loading",
      },
    ],
  };
  componentDidMount() {
    if (this.props.searchString === "") {
      axios("https://hn.algolia.com/api/v1/search?tags=front_page").then(
        (res) => {
          this.setState({
            data: res.data["hits"],
          });
        }
      );
    }
  }

  componentDidUpdate() {
    axios(
      `https://hn.algolia.com/api/v1/search?query=${this.props.searchString}&tags=story`
    ).then((res) => {
      this.setState({
        data: res.data["hits"],
      });
    });
  }
  render() {
    return (
      <div style={bodyStyle}>
        <section className="search_engine d-flex justify-content-between mx-3 py-2">
          <div id="selector" className="d-flex">
            <div>
              Search
              <select id="to_search" className="mx-2" defaultValue="Stories">
                <option>Stories</option>
                <option>All</option>
                <option>Comments</option>
              </select>
            </div>

            <div>
              by
              <select id="by" className="mx-2" defaultValue="Stories">
                <option>Popularity</option>
                <option>Date</option>
              </select>
            </div>

            <div>
              for
              <select id="time_range" className="mx-2" defaultValue="Stories">
                <option>All Time</option>
                <option>Last 24h</option>
                <option>Past Week</option>
                <option>Past Month</option>
                <option>Past Year</option>
                <option>Custom Range</option>
              </select>
            </div>
          </div>
          <div>
            <img src={share} alt="" style={{ marginRight: "auto" }} />
          </div>
        </section>
        <Content data={this.state.data} />
      </div>
    );
  }
}
const bodyStyle = {
  color: "white",
  backgroundColor: "black",
  margin: "0 2rem 0 2rem",
  minHeight: "90vh",
};

export default Body;
