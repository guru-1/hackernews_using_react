import React, { Component } from "react";
import { Card } from "react-bootstrap";

export class Dataitem extends Component {
  render() {
    const { data } = this.props;
    return (
      <div>
        {data.length === 1 ? (
          <h1 style={{ color: "white", textAlign: "center" }}>Loading...</h1>
        ) : (
          data.map((item, index) =>
            ["Dark"].map((variant, idx) => (
              <Card
                bg={variant.toLowerCase()}
                key={idx}
                text={variant.toLowerCase() === "light" ? "dark" : "white"}
                style={{ width: "auto", textAlign: "center" }}
                className="mb-2 center"
              >
                <Card.Header>{index + 1}</Card.Header>
                <Card.Body>
                  <Card.Title>
                    <h1>{item.title}</h1>
                  </Card.Title>
                  <Card.Text>
                    <div
                      className="content-div py-2 px-2"
                      style={{ color: "white" }}
                    >
                      <div>
                        <h2>
                          {item.num_comments} | {item.author} |{" "}
                          {item.created_at} | {item.points} |{" "}
                        </h2>
                      </div>
                      <div>
                        <a
                          href={item.url}
                          style={{ color: "orange", textDecoration: "none" }}
                        >
                          Visit the page!
                        </a>
                      </div>
                    </div>
                  </Card.Text>
                </Card.Body>
              </Card>
            ))
          )
        )}
      </div>
    );
  }
}

export default Dataitem;
